export const environment = {
  production: false,
  apiUrl: 'http://10.4.4.92:94/',
  application:
  {
    name: 'angular-starter',
    angular: 'Angular 9.0.6',
    bootstrap: 'Bootstrap 4.4.1',
    fontawesome: 'Font Awesome 5.12.1',
  }
};