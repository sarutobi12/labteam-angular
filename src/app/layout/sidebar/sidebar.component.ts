import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  role: any
  constructor() { }

  ngOnInit(): void {
    this.role = Number(localStorage.getItem('role'))
  }
  returnhome(){
    window.open('home','_blank')
  }
}
