import { AuthService } from './../../_core/_services/auth.service';
import { Component, OnInit } from '@angular/core';
import { Router , ActivatedRoute } from '@angular/router';
import * as moment from 'moment-timezone';
@Component({
  selector: 'app-site-header',
  templateUrl: './site-header.component.html',
  styleUrls: ['./site-header.component.css']
})
export class SiteHeaderComponent implements OnInit {
  currentTime: null
  username: any
  constructor(
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.currentTime = moment().format('LTS');
    setInterval(() => this.updateCurrentTime(), 1 * 1000)
    this.username = localStorage.getItem('user')
  }
  updateCurrentTime() {
    this.currentTime = moment().format('LTS');
  }
  logout(){
    this.authService.logout()
    const uri = this.router.url;
    this.router.navigate(['login'],{queryParams:{uri}})
  }
}
