import { ArticleComponent } from './views/article/article.component';
import { TeamMemberComponent } from './views/team-member/team-member.component';
import { OkrComponent } from './views/okr/okr.component';
import { TrainingComponent } from './views/training/training.component';
import { CarouselComponent } from './views/carousel/carousel.component';
import { ProjectComponent } from './views/project/project.component';
import { LoginComponent } from './modules/general/login/login.component';
import { AuthGuard } from './_core/_guards/auth.guard';
import { DefaultComponent } from './layout/default/default.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './modules/general/home/home.component';
import { NotFoundComponent } from './modules/general/not-found/not-found.component';
const routes: Routes = [
  { path: '',
    redirectTo: '/home',
    pathMatch: 'full',
  },
  { path: 'home', component: HomeComponent, },

  {
    path: 'admin',
    component: DefaultComponent,
    children: [
      { path: '',component: ProjectComponent,canActivate: [AuthGuard],
        data: {
          title: 'admin'
        }
      }
    ]
  },
  {
    path: 'admin-project',
    component: DefaultComponent,
    children: [
      { path: '',component: ProjectComponent,canActivate: [AuthGuard],
        data: {
          title: 'Project'
        }
      }
    ]
  },

  {
    path: 'admin-slide',
    component: DefaultComponent,
    children: [
      { path: '' , component: CarouselComponent, canActivate: [AuthGuard],
        data: {
          title: 'Carousel'
        }
      }
    ]
  },

  {
    path: 'traning-resources',
    component: DefaultComponent,
    children: [
      { path: '' , component: TrainingComponent, canActivate: [AuthGuard],
        data: {
          title: 'Training Resources'
        }
      }
    ]
  },

  {
    path: 'okr',
    component: DefaultComponent,
    children: [
      { path: '' , component: OkrComponent, canActivate: [AuthGuard],
        data: {
          title: 'Objectives Key Results'
        }
      }
    ]
  },

  {
    path: 'team-member',
    component: DefaultComponent,
    children: [
      { path: '' , component: TeamMemberComponent, canActivate: [AuthGuard],
        data: {
          title: 'Team Member'
        }
      }
    ]
  },
  {
    path: 'article',
    component: DefaultComponent,
    children: [
      { path: '' , component: ArticleComponent, canActivate: [AuthGuard],
        data: {
          title: 'Article'
        }
      }
    ]
  },
  {
    path: 'login',
    component: LoginComponent
  },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }