import { User } from './../_model/user';
import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable, of, throwError } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
const httpOptions = {
  headers: new HttpHeaders(
    {
      'Content-Type': 'application/json',
    }
  )
}
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  jwtHelper = new JwtHelperService();
  currentDate: any = Date.now() ;
  currentUser: User;
  decodedToken: any;
  userid: any
  constructor(private http: HttpClient) { }
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }
  login(model: any){
    return this.http.post('http://10.4.0.76:94/api/Auth/login',model)
      .pipe(map((response: any) =>{
        const user = response;
        if (user) {
          localStorage.setItem('token', user.token);
          localStorage.setItem('user', user.user.User.Username);
          localStorage.setItem('role', user.user.User.Role);
          localStorage.setItem('authTokenExpiration', this.currentDate + 14400000);
          this.decodedToken = this.jwtHelper.decodeToken(user.token.nameid);
          this.userid = Number(this.jwtHelper.decodeToken(localStorage.getItem('token')).nameid)
          // console.log(this.userid)
          this.currentUser = user.user;
        }
      }))
  }
  login2(url: string, users: User): Observable<User>{
    return this.http.post<User>(url,users,httpOptions)
  }
  loggedIn() {
    const token = localStorage.getItem('token');
    const user = localStorage.getItem('user');
    const expiration = localStorage.getItem('authTokenExpiration');
    if (!token || !expiration || !user) return null;
    if (Date.now() > parseInt(expiration,10)) {
      this.destroyToken();
      return null;
    } else {
      return !this.jwtHelper.isTokenExpired(token);
    }
  }
  logout(){
    this.destroyToken()
  }
  destroyToken(){
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    localStorage.removeItem('role');
    localStorage.removeItem('authTokenExpiration');
  }
}
