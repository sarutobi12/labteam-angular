import { environment } from './../../../environments/environment';
import { CarouselService } from './../../_core/_services/carousel.service';
import { Carousel } from './../../_core/_model/carousel';
import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute } from '@angular/router';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { NgbModal , NgbModalRef  } from '@ng-bootstrap/ng-bootstrap';
import  swal  from 'sweetalert2';
@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.css']
})
export class CarouselComponent implements OnInit {
  baseUrl = environment.apiUrl
  modalReference: NgbModalRef
  data: Carousel[] = []
  title: any
  subtitle: any
  TitleEdit: any
  SubTitleEdit: any
  img: any
  ID: any
  file: File = null
  file2: File = null
  previewUrl:any = null
  fileUploadProgress: string = null
  uploadedFilePath: string = null
  previewUrl2:any = null
  fileUploadProgress2: string = null
  uploadedFilePath2: string = null
  constructor(
    private carouselService: CarouselService,
    private modalService: NgbModal,
    private router: Router,
    private route: ActivatedRoute,
    private http: HttpClient,
  ) { }

  ngOnInit(): void {
    this.GetAllSlide()
  }
  GetAllSlide(){
    this.carouselService.getAll(this.baseUrl + 'api/Slides/GetListSlide')
      .subscribe((res: any)=>{
        this.data = res
    })
  }
  openLg(content){
    this.modalReference = this.modalService.open(content,{ size: 'lg' })
  }
  openLgedit(edit) {
    this.modalReference = this.modalService.open(edit,{ size: 'lg' });
  }
  save(){
    const formData = new FormData()
    formData.append('UploadedFile', this.file)
    formData.append('UploadedFileTitle', this.title)
    formData.append('UploadedFileSubtitle', this.subtitle)
    this.http.post(this.baseUrl + 'api/slides/Created', formData)
      .subscribe((res: any)=>{
        const Toast = swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 3000,
          timerProgressBar: false,
          onOpen: (toast) => {
            toast.addEventListener('mouseenter', swal.stopTimer)
            toast.addEventListener('mouseleave', swal.resumeTimer)
          }
        })
        Toast.fire({
          icon: 'success',
          title: 'Add Slide successfully'
        })
        this.modalReference.close()
        this.GetAllSlide()
      })
  }
  Editslide(item,edit){
    this.ID = item.ID
    this.TitleEdit = item.Title
    this.SubTitleEdit = item.Subtitle
    this.img = item.Image
    this.openLgedit(edit)
  }
  update(){
    const formData = new FormData()
    formData.append('UploadedFile', this.file2)
    formData.append('UploadedFileTitleEdit', this.TitleEdit)
    formData.append('UploadedFileSubtitleEdit', this.SubTitleEdit)
    formData.append('UploadedFileID', this.ID)
    this.http.post(this.baseUrl + 'api/Slides/Update', formData)
      .subscribe((res: any)=>{
        const Toast = swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 3000,
          timerProgressBar: false,
          onOpen: (toast) => {
            toast.addEventListener('mouseenter', swal.stopTimer)
            toast.addEventListener('mouseleave', swal.resumeTimer)
          }
        })
        Toast.fire({
          icon: 'success',
          title: 'Update Slide successfully'
        })
        this.modalReference.close()
        this.GetAllSlide()
      })
  }
  deleteslide(ID){
    swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.carouselService.delete(this.baseUrl + `api/Slides/DeleteSlide/${ID}`)
        .subscribe((res: any)=>{
          swal.fire(
            'Deleted!',
            'Your Slide has been deleted.',
            'success'
          )
          this.GetAllSlide()
        })
      }
    })
  }
  fileProgress(event){
    this.file = event.target.files[0];
    this.preview();
  }
  preview() {
    const mimeType = this.file.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }
    const reader = new FileReader()
    reader.readAsDataURL(this.file)
    reader.onload = (_event) => {
      this.previewUrl = reader.result
    }
  }
  preview2() {
    const mimeType = this.file2.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }
    const reader = new FileReader()
    reader.readAsDataURL(this.file2)
    reader.onload = (_event) => {
      this.previewUrl2 = reader.result
      this.img = reader.result
    }
  }
  fileProgress2(event){
    this.file2 = event.target.files[0];
    this.preview2();
  }
}
