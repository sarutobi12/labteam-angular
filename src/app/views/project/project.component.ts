import  swal  from 'sweetalert2';
import { environment } from './../../../environments/environment';
import { Project } from './../../_core/_model/project';
import { ProjectService } from './../../_core/_services/project.service';
import { Component, OnInit , ViewEncapsulation  } from '@angular/core';
import {Router, ActivatedRoute } from '@angular/router';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { NgbModal , NgbModalRef  } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
const Toast = swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000,
  timerProgressBar: false,
  onOpen: (toast) => {
    toast.addEventListener('mouseenter', swal.stopTimer)
    toast.addEventListener('mouseleave', swal.resumeTimer)
  }
})
@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css'],
  encapsulation: ViewEncapsulation.None,
  providers: [DatePipe]
})
export class ProjectComponent implements OnInit {
  modalReference: NgbModalRef
  baseUrl = environment.apiUrl
  data: Project[] = []
  file: File = null
  file2: File = null
  previewUrl:any = null
  fileUploadProgress: string = null
  uploadedFilePath: string = null
  previewUrl2:any = null
  fileUploadProgress2: string = null
  uploadedFilePath2: string = null
  name: any
  nameEdit: any
  URL: any
  ID: any
  URLEdit: any
  img: any
  WorkBy: any
  WorkByEdit: any
  constructor(
    private modalService: NgbModal,
    private datePipe: DatePipe,
    private router: Router,
    private route: ActivatedRoute,
    private http: HttpClient,
    private projectService: ProjectService
  ) { }

  ngOnInit(): void {
    this.getAll()
  }
  getAll(){
    this.projectService.getAll(this.baseUrl + 'api/Projects/GetListProject')
      .subscribe((res: any)=>{
        this.data = res
      })
  }
  fileProgress(event) {
    this.file = event.target.files[0];
    this.preview();
  }
  fileProgress2(event) {
    this.file2 = event.target.files[0];
    this.preview2();
  }
  preview() {
    const mimeType = this.file.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }
    const reader = new FileReader()
    reader.readAsDataURL(this.file)
    reader.onload = (_event) => {
      this.previewUrl = reader.result
    }
  }
  preview2() {
    const mimeType = this.file2.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }
    const reader = new FileReader()
    reader.readAsDataURL(this.file2)
    reader.onload = (_event) => {
      this.previewUrl2 = reader.result
      this.img = reader.result
    }
  }
  save(){
    const formData = new FormData()
    formData.append('UploadedFile', this.file)
    formData.append('UploadedFileName', this.name)
    formData.append('UploadedFileURL', this.URL)
    formData.append('UploadedFileWorkBy', this.WorkBy)
    this.http.post(this.baseUrl + 'api/projects/Created2', formData)
      .subscribe((res: any)=>{
        Toast.fire({
          icon: 'success',
          title: 'Add Project successfully'
        })
        this.modalReference.close()
        this.getAll()
      })
  }
  openLg(content) {
    this.modalReference = this.modalService.open(content,{ size: 'lg' });
  }
  Editproject(item,edit){
    this.ID = item.ID
    this.nameEdit = item.Name
    this.URLEdit = item.URL
    this.img= item.Image;
    this.WorkByEdit = item.WorkBy
    this.openLgedit(edit)
  }
  openLgedit(edit) {
    this.modalReference = this.modalService.open(edit,{ size: 'lg' });
  }
  update(){
    const formData = new FormData()
    formData.append('UploadedFile', this.file2);
    formData.append('UploadedFilenameEdit', this.nameEdit);
    formData.append('UploadedFileURLEdit', this.URLEdit);
    formData.append('UploadedFileWorkByEdit', this.WorkByEdit);
    formData.append('UploadedFileID', this.ID);
    this.http.post(this.baseUrl + 'api/Projects/Update', formData)
      .subscribe((res=>{
        Toast.fire({
          icon: 'success',
          title: 'Update Project successfully'
        })
        this.modalReference.close()
        this.getAll()
      }))
  }
  delete(ID){
    swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.projectService.delete(this.baseUrl + `api/Projects/DeleteProject/${ID}`)
        .subscribe((res: any)=>{
          Toast.fire({
            icon: 'success',
            title: 'Delete Project successfully'
          })
          this.getAll()
        })
      }
    })
  }
}
