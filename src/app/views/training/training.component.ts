import { Training } from './../../_core/_model/training';
import { TrainingService } from './../../_core/_services/training.service';
import { Component, OnInit, ViewChild  } from '@angular/core';
import {Router, ActivatedRoute } from '@angular/router';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { SortService,ToolbarService, PageService, EditService, ExcelExportService, PdfExportService, ContextMenuService } from '@syncfusion/ej2-angular-treegrid';
import {  EditSettingsModel } from '@syncfusion/ej2-treegrid';
import { ClickEventArgs } from '@syncfusion/ej2-navigations';
import { TreeGridComponent } from '@syncfusion/ej2-angular-treegrid';
import { FilterService } from '@syncfusion/ej2-angular-treegrid';
import { NgbModal , NgbModalRef  } from '@ng-bootstrap/ng-bootstrap';
import  swal  from 'sweetalert2';
import { environment } from './../../../environments/environment';
import { JwtHelperService } from '@auth0/angular-jwt';
import { DomSanitizer} from '@angular/platform-browser';
import { ClipboardService } from 'ngx-clipboard'
@Component({
  selector: 'app-training',
  templateUrl: './training.component.html',
  styleUrls: ['./training.component.css'],
  providers: [SortService, ToolbarService, PageService, EditService, ExcelExportService, PdfExportService, ContextMenuService,FilterService]
})
export class TrainingComponent implements OnInit {
  baseUrl = environment.apiUrl
  jwtHelper = new JwtHelperService();
  modalReference: NgbModalRef
  searchSettings: any = { hierarchyMode: 'Parent' }
  public treegrid: TreeGridComponent
  editparams: any = { params: { format: 'n' } }
  contextMenuItems: any = [
    {
      text: 'Add Function Project',
      iconCss: 'e-icons e-add',
      target: '.e-content',
      id: 'Add-Sub-OC'
    },
    {
      text: 'Edit Function Project',
      iconCss: ' e-icons e-edit',
      target: '.e-content',
      id: 'EditOC'
    },
    {
      text: 'Delete',
      iconCss: ' e-icons e-delete',
      target: '.e-content',
      id: 'DeleteOC'
    }
  ]
  toolbar: any = [
    'Search',
    'ExpandAll',
    'CollapseAll',
  ]
  editing: any = { allowDeleting: true, allowEditing: true, mode: 'Row' }
  public pageSettings: any = { pageSize: 100 }
  expanded: any = {}
  data: Training[] = []
  tasks = []
  name: any
  Editname: any
  pathlink = null
  Editpath = null
  file: File = null
  file2: File = null
  public src: any
  id: any
  title = null
  oc: any = { id: 0, name: '' , link: '', level: 0, parentid: 0 }
  edit: any = {
    key: 0,
    title: '',
    link: '',
    path: ''
  }
  modalTitle: any = 'Add Project Training'
  constructor(
    private clipboardService: ClipboardService,
    public sanitizer: DomSanitizer,
    private modalService: NgbModal,
    private router: Router,
    private route: ActivatedRoute,
    private http: HttpClient,
    private trainingService: TrainingService
  ) { }

  ngOnInit(): void {
    this.getListTraining()
  }

  getListTraining(){
    this.trainingService.getTraining(this.baseUrl + `api/Ocs/GetListTreeByUser/${Number(this.jwtHelper.decodeToken(localStorage.getItem('token')).nameid)}`)
      .subscribe((res: any)=>{
        this.tasks = res
        this.data = res
    })
  }

  rowSelected(args){
    this.src = args.data.link
    console.log(this.src)
    this.title = args.data.title
    this.edit = {
      key: args.data.key,
      title: args.data.title
    };
    this.oc = {
      id: args.data.key,
      name: args.data.title,
      level: args.data.levelnumber,
      parentid: args.data.parentid
    }
  }

  actionComplete(args){
    if (args.requestType === 'save') {
      this.edit.title = args.data.title;
      this.edit.link = args.data.link;
      this.edit.path = args.data.linkpath;
      this.http.post(this.baseUrl +'api/Ocs/Rename', {
        key: this.edit.key,
        title: this.edit.title,
        link: this.edit.link,
        path: this.edit.path
      }).subscribe(res => {
        const Toast = swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 3000,
          timerProgressBar: false,
          onOpen: (toast) => {
            toast.addEventListener('mouseenter', swal.stopTimer)
            toast.addEventListener('mouseleave', swal.resumeTimer)
          }
        })
        Toast.fire({
          icon: 'success',
          title: 'Edit Successfully'
        })
      });
    }
  }

  contextMenuClick(args,addfunction,edit){
    if (args.item.id === 'EditOC') {
      this.modalTitle = 'Edit Function Project'
      this.openLgEdit(edit)
      this.Editname = this.oc.name
      this.Editpath = args.rowInfo.rowData.linkpath
      this.id = this.oc.id
      console.log(this.oc)
    } else if (args.item.id === 'DeleteOC') {
      this.delete(args.rowInfo.rowData.key)
    } else {
      this.modalTitle = 'Add Function-Project'
      this.openLgAddFunction(addfunction)
      this.oc.parentid = args.rowInfo.rowData.key
    }
  }

  watchvideo(video){
    // this.openLgVideo(video)
    this.modalReference = this.modalService.open(video,{ size: 'xl' });
  }

  openLg(addModel){
    this.modalReference = this.modalService.open(addModel,{ size: 'lg' });
  }
  
  openLgAddFunction(addfunction){
    this.modalReference = this.modalService.open(addfunction,{ size: 'lg' });
  }

  openLgEdit(edit){
    this.modalReference = this.modalService.open(edit,{ size: 'lg' });
  }

  openLgVideo(video){
    this.modalReference = this.modalService.open(video,{ size: 'xl' });
  }

  fileProgress(event){
    this.file = event.target.files[0];
  }

  save(){
    if (this.oc.parentid > 0) {
      const formData = new FormData();
      formData.append('UploadedFile', this.file);
      formData.append('UploadedFileName', this.name);
      formData.append('UploadedFileLevel', this.oc.level + 1);
      formData.append('UploadedFileParentID', this.oc.parentid);
      formData.append('UploadedFilePath', this.pathlink);
      formData.append('UploadedUser', this.jwtHelper.decodeToken(localStorage.getItem('token')).nameid);
      this.http.post(this.baseUrl + 'api/Ocs/CreateSubOC2',formData).subscribe(res => {
        if (res) {
          const Toast = swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: false,
            onOpen: (toast) => {
              toast.addEventListener('mouseenter', swal.stopTimer)
              toast.addEventListener('mouseleave', swal.resumeTimer)
            }
          })
          Toast.fire({
            icon: 'success',
            title: 'Add Function Project Training successfully'
          })
          this.modalReference.close()
          this.getListTraining()
          this.clearFrom()
        }
      })
    } else {
      const formData = new FormData();
      formData.append('UploadedFile', this.file);
      formData.append('UploadedFileName', this.name);
      formData.append('UploadedFileParentID', this.oc.parentid);
      formData.append('UploadedFilePath', this.pathlink);
      formData.append('UploadedUser', this.jwtHelper.decodeToken(localStorage.getItem('token')).nameid);
      this.http.post(this.baseUrl + 'api/Ocs/CreateOC2',formData).subscribe(res => {
        if(res) {
          const Toast = swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: false,
            onOpen: (toast) => {
              toast.addEventListener('mouseenter', swal.stopTimer)
              toast.addEventListener('mouseleave', swal.resumeTimer)
            }
          })
          Toast.fire({
            icon: 'success',
            title: 'Add Project Training successfully'
          })
          this.modalReference.close()
          this.clearFrom()
          this.getListTraining();
        }
      })
    }
  }

  delete(id){
    this.http.delete(this.baseUrl + `api/Ocs/Delete/${id}`).subscribe(res => {
      if (res) {
        this.getListTraining();
      }
      const Toast = swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: false,
        onOpen: (toast) => {
          toast.addEventListener('mouseenter', swal.stopTimer)
          toast.addEventListener('mouseleave', swal.resumeTimer)
        }
      })
      Toast.fire({
        icon: 'success',
        title: 'Delete successfully'
      })
    })
  }

  update(){
    const formData = new FormData();
    formData.append('UploadedFile', this.file);
    formData.append('UploadedFileName', this.Editname);
    formData.append('UploadedFilePath', this.Editpath);
    formData.append('UploadedFileID', this.id);
    this.http.post(this.baseUrl + 'api/Ocs/Rename2',formData).subscribe(res => {
      if (res) {
        this.modalReference.close()
        this.getListTraining()
      }
      const Toast = swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: false,
        onOpen: (toast) => {
          toast.addEventListener('mouseenter', swal.stopTimer)
          toast.addEventListener('mouseleave', swal.resumeTimer)
        }
      })
      Toast.fire({
        icon: 'success',
        title: 'Function Project Update Success'
      })
    })
  }

  clearFrom() {
    this.oc = { id: 0, name: '', level: 0 }
    this.name= ''
    this.pathlink = ''
    this.file = null
  }

  open(linkpath){
    swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: 'Browser is not allowed to access local resources , Please copy Link Path paste to the Windows Explorer!',
    })
  }
  copy(linkpath){
    this.clipboardService.copyFromContent(linkpath)
    swal.fire(
      'Copy success!',
      'copy successfully : ' + ' ' + linkpath,
      'success'
    )
  }
}
