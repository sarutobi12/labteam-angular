import { Okr } from './../../_core/_model/okr';
import { OkrService } from './../../_core/_services/okr.service';
import { Component, OnInit } from '@angular/core';
import { SortService,ToolbarService, PageService, EditService, ExcelExportService, PdfExportService, ContextMenuService } from '@syncfusion/ej2-angular-treegrid';
import { TreeGridComponent } from '@syncfusion/ej2-angular-treegrid';
import { FilterService } from '@syncfusion/ej2-angular-treegrid';
import {  EditSettingsModel } from '@syncfusion/ej2-treegrid';
import { ClickEventArgs } from '@syncfusion/ej2-navigations';
import { NgbModal , NgbModalRef  } from '@ng-bootstrap/ng-bootstrap';
import {Router, ActivatedRoute } from '@angular/router';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { environment } from './../../../environments/environment';

import  swal  from 'sweetalert2';
@Component({
  selector: 'app-okr',
  templateUrl: './okr.component.html',
  styleUrls: ['./okr.component.css'],
  providers: [SortService, ToolbarService, PageService, EditService, ExcelExportService, PdfExportService, ContextMenuService,FilterService]
})

export class OkrComponent implements OnInit {
  baseUrl = environment.apiUrl
  modalReference: NgbModalRef
  searchSettings: any = { hierarchyMode: 'Parent' }
  public treegrid: TreeGridComponent
  editparams: any = { params: { format: 'n' } }
  contextMenuItems: any = [
    {
      text: 'Add Detail OKR',
      iconCss: ' e-icons e-add',
      target: '.e-content',
      id: 'AddDetailOKR'
    }
  ]
  toolbar: any = [
    'Search',
    'ExpandAll',
    'CollapseAll',
  ]
  editing: any = { allowDeconsting: true, allowEditing: true, mode: 'Row' }
  public pageSettings: any = { pageSize: 100 }
  expanded: any = {}
  data: Okr[] = []
  oc: any = { id: 0, name: '' , link: '', level: 0, parentid: 0 }
  edit: any = {
    key: 0,
    title: '',
    link: '',
    path: ''
  }
  modalTitle: any = 'Add OKR'
  name: any
  id: any
  confi: any
  okrid: any
  teamhe: any
  prog: any
  next: any
  inputs1: any = [
    {
      name: ''
    }
  ]
  inputs2: any = [
    {
      name: ''
    }
  ]
  inputs3: any = [
    {
      name: ''
    }
  ]
  inputs4: any = [
    {
      name: ''
    }
  ]
  contents: any
  Confidence: any = []
  TeamHealth: any = []
  Progress: any = []
  NextWeek: any = []
  options: any = []
  title: any
  categoryid: any
  categoryid2: any
  categoryid3: any
  categoryid4: any

  constructor(
    private modalService: NgbModal,
    private router: Router,
    private route: ActivatedRoute,
    private http: HttpClient,
    private okrService: OkrService
  ) { }

  ngOnInit(): void {
    this.getListOKr()
    this.GetOKRCategory()
  }
  OpenDetail(key,detail){
    this.openLgDetail(detail)
    this.getDetailOKR(key)
  }
  Delete(key){
    swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.http.delete(this.baseUrl + `api/OKRs/DeleteOkr/${key}`)
        .subscribe((res: any)=>{
          swal.fire(
            'Deleted!',
            'Your OKR has been deleted.',
            'success'
          )
          this.getListOKr()
        })
      }
    })
  }
  EditFunction(key,editfunction){
    this.openLgeditFunction(editfunction)
    this.GetOKRCategory()
    this.getDetailOKR(key)
  }
  rowSelected(args){
    this.okrid = args.data.key
    this.title = args.data.title
    this.edit = {
      key: args.data.key,
      title: args.data.title
    };
    this.oc = {
      id: args.data.key,
      name: args.data.title,
      level: args.data.levelnumber,
      parentid: args.data.parentid
    }
  }
  actionComplete(args){
    if (args.requestType === 'save') {
      this.edit.title = args.data.title;
      this.http
      .post(this.baseUrl + 'api/okrs/Rename', {
        key: this.edit.key,
        title: this.edit.title,
      })
      .subscribe(res => {
        const Toast = swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 3000,
          timerProgressBar: true,
          onOpen: (toast) => {
            toast.addEventListener('mouseenter', swal.stopTimer)
            toast.addEventListener('mouseleave', swal.resumeTimer)
          }
        })
        Toast.fire({
            icon: 'success',
            title: 'successfully'
        })
      });
    }
  }
  contextMenuClick(args,addModel,addfunction){
    if (args.item.id === 'EditOC') {
    } else if (args.item.id === 'DeconsteOC') {
    } else if(args.item.id === 'AddDetailOKR'){
        this.modalTitle = 'Add Detail OKR'
        this.openLgFunction(addfunction)
        this.GetOKRCategory()
    } else {
      this.modalTitle = 'Add OKR'
      this.oc.parentid = args.rowInfo.rowData.key
      this.openLg(addModel)
      // console.log(this.oc);
    }
  }
  getListOKr() {
    this.okrService.getListokr(this.baseUrl + 'api/OKRs/GetListTree').subscribe((res: any) => {
      this.data = res
    })
  }
  getDetailOKR(key){
    this.http.get(this.baseUrl +`api/OKRs/GetDetail/${key}`).subscribe((res: any) =>{
        this.Confidence = res.Confidence
        this.TeamHealth = res.Teamhealth
        this.Progress = res.Progess
        this.NextWeek = res.Nextweek
    })
  }
  GetOKRCategory(){
    this.http.get(this.baseUrl + 'api/OKRs/GetOKRCategory').subscribe((res: any)=>{
        // console.log(r)
        this.options = res
        this.confi = res[0].Name
        this.categoryid = res[0].ID
        this.teamhe = res[1].Name
        this.categoryid2 = res[1].ID
        this.prog = res[2].Name
        this.categoryid3 = res[2].ID
        this.next = res[3].Name
        this.categoryid4 = res[3].ID
    })
  }
  save(){
    if (this.oc.parentid > 0) {
      this.http
        .post(this.baseUrl + 'api/OKRs/CreatesubOC', {
          name: this.name,
          level: this.oc.level + 1,
          parentid: this.oc.parentid,
        })
        .subscribe(res => {
          if (res) {
            this.clearFrom();
            this.modalReference.close()
            this.getListOKr()
          }
          const Toast = swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: false,
            onOpen: (toast) => {
              toast.addEventListener('mouseenter', swal.stopTimer)
              toast.addEventListener('mouseleave', swal.resumeTimer)
            }
          })
          Toast.fire({
            icon: 'success',
            title: 'New Function Project Add Success'
          })
        });
    } else {
      this.http
        .post(this.baseUrl + 'api/OKRs/CreateOC', {
          name: this.name,
          parentid: this.oc.parentid
        })
        .subscribe(res => {
          if (res) {
            this.clearFrom();
            this.modalReference.close()
            this.getListOKr()
            const Toast = swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 3000,
              timerProgressBar: false,
              onOpen: (toast) => {
                toast.addEventListener('mouseenter', swal.stopTimer)
                toast.addEventListener('mouseleave', swal.resumeTimer)
              }
            })
            Toast.fire({
              icon: 'success',
              title: 'New OKR Add Success'
            })
          }
          console.log(res);
        });
    }
  }
  clearFrom() {
    this.oc = { id: 0, name: '', level: 0 } ;
    (this.name = '')
  }
  openLg(addModel){
    this.modalReference = this.modalService.open(addModel,{ size: 'xl' });
  }
  openLgFunction(addfunction){
    this.modalReference = this.modalService.open(addfunction,{ size: 'xl' })
  }
  openLgeditFunction(editfunction){
    this.modalReference = this.modalService.open(editfunction,{ size: 'xl' })
  }
  openLgDetail(detail){
    this.modalReference = this.modalService.open(detail,{ size: 'xl' })
  }
  keyup(e){
    this.contents= this.inputs1.map(item=>{
        return item.name;
    }).join('@@##')
  }
  keyup1(e){
    this.contents= this.Confidence.map(item=>{
        return item.Content;
    }).join('@@##')
  }
  removedetail1(index) {
    this.inputs1.splice(index, 1)
  }
  removedetail2(index) {
    this.inputs3.splice(index, 1);
  }
  removedetail3(index) {
    this.inputs4.splice(index, 1);
  }
  addrowdetail1(index){
    this.inputs1.push({ name: '' });
  }
  addrowdetail2(index){
    this.inputs3.push({ name: '' });
  }
  addrowdetail3(index){
      this.inputs4.push({ name: '' });
  }
  addDetail1(){
    const arrcontent = this.inputs1.map(item=>{
      return item.name
    }).join('@@##')
    this.http.post(this.baseUrl +'api/OKRs/Add',{
      arrContent: arrcontent,
      CategoryID: this.categoryid,
      OKRID: this.okrid
    }).subscribe(r=>{
      const Toast = swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: false,
        onOpen: (toast) => {
          toast.addEventListener('mouseenter', swal.stopTimer)
          toast.addEventListener('mouseleave', swal.resumeTimer)
        }
      })
      Toast.fire({
        icon: 'success',
        title: 'successfully'
      })
      this.inputs1 = [
        {
        name: ''
        }
      ]
    })
  }
  addDetail2(){
    const arrcontent = this.inputs2.map(item=>{
        return item.name
    }).join('@@##')
    this.http.post(this.baseUrl + 'api/OKRs/Add',{
      arrContent: arrcontent,
      CategoryID: this.categoryid2,
      OKRID: this.okrid
    }).subscribe(r=>{
      const Toast = swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: false,
        onOpen: (toast) => {
          toast.addEventListener('mouseenter', swal.stopTimer)
          toast.addEventListener('mouseleave', swal.resumeTimer)
        }
      })
      Toast.fire({
        icon: 'success',
        title: 'successfully'
      })
      this.inputs2 = [
        {
        name: ''
        }
      ]
    })
  }
  addDetail3(){
    const arrcontent = this.inputs3.map(item=>{
        return item.name
    }).join('@@##')
    this.http.post(this.baseUrl + 'api/OKRs/Add',{
      arrContent: arrcontent,
      CategoryID: this.categoryid3,
      OKRID: this.okrid
    }).subscribe(r=>{
      const Toast = swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: false,
        onOpen: (toast) => {
          toast.addEventListener('mouseenter', swal.stopTimer)
          toast.addEventListener('mouseleave', swal.resumeTimer)
        }
      })
      Toast.fire({
        icon: 'success',
        title: 'successfully'
      })
      this.inputs3 = [
        {
        name: ''
        }
      ]
    })
  }
  addDetail4(){
    const arrcontent = this.inputs4.map(item=>{
      return item.name
    }).join('@@##')
    this.http.post(this.baseUrl + 'api/OKRs/Add',{
      arrContent: arrcontent,
      CategoryID: this.categoryid4,
      OKRID: this.okrid
    }).subscribe(r=>{
      const Toast = swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: false,
        onOpen: (toast) => {
          toast.addEventListener('mouseenter', swal.stopTimer)
          toast.addEventListener('mouseleave', swal.resumeTimer)
        }
      })
      Toast.fire({
        icon: 'success',
        title: 'successfully'
      })
      this.inputs4 = [
        {
        name: ''
        }
     ]
    })
  }
  saveOKR1(){
    const arr = this.Confidence.map(item=>{
        return item.ID
    }).join('@@##')
    const arrcontent = this.Confidence.map(item=>{
        return item.Content
    }).join('@@##')
    this.http.post(this.baseUrl + 'api/okrs/updateokr',{
        arrID: arr,
        CategoryID: this.categoryid,
        arrContent: arrcontent,
        okrid : this.okrid
    }).subscribe((r: any)=>{
      const Toast = swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: false,
        onOpen: (toast) => {
          toast.addEventListener('mouseenter', swal.stopTimer)
          toast.addEventListener('mouseleave', swal.resumeTimer)
        }
      })
      Toast.fire({
          icon: 'success',
          title: 'successfully'
      })
      this.getDetailOKR(this.okrid)
    })
  }
  saveOKR2(){
    const arr = this.TeamHealth.map(item=>{
      return item.ID
    }).join('@@##')
    const arrcontent = this.TeamHealth.map(item=>{
      return item.Content
    }).join('@@##')
    this.http.post(this.baseUrl + 'api/okrs/updateokr',{
      arrID: arr,
      CategoryID: this.categoryid2,
      arrContent: arrcontent,
      okrid : this.okrid
    }).subscribe((r: any)=>{
      const Toast = swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: false,
        onOpen: (toast) => {
          toast.addEventListener('mouseenter', swal.stopTimer)
          toast.addEventListener('mouseleave', swal.resumeTimer)
        }
      })
        Toast.fire({
          icon: 'success',
          title: 'successfully'
        })
      this.getDetailOKR(this.okrid)
    })
  }
  saveOKR3(){
    const arr = this.Progress.map(item=>{
      return item.ID
    }).join('@@##')
    const arrcontent = this.Progress.map(item=>{
      return item.Content
    }).join('@@##')
    this.http.post(this.baseUrl + 'api/okrs/updateokr',{
      arrID: arr,
      CategoryID: this.categoryid3,
      arrContent: arrcontent,
      okrid : this.okrid
    }).subscribe((r: any)=>{
      const Toast = swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: false,
        onOpen: (toast) => {
          toast.addEventListener('mouseenter', swal.stopTimer)
          toast.addEventListener('mouseleave', swal.resumeTimer)
        }
      })
      Toast.fire({
        icon: 'success',
        title: 'successfully'
      })
      this.getDetailOKR(this.okrid)
    })
  }
  saveOKR4(){
    const arr = this.NextWeek.map(item=>{
      return item.ID
    }).join('@@##')
    const arrcontent = this.NextWeek.map(item=>{
      return item.Content
    }).join('@@##')
    this.http.post(this.baseUrl + 'api/okrs/updateokr',{
      arrID: arr,
      CategoryID: this.categoryid4,
      arrContent: arrcontent,
      okrid : this.okrid
    }).subscribe((r: any)=>{
      const Toast = swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: false,
        onOpen: (toast) => {
          toast.addEventListener('mouseenter', swal.stopTimer)
          toast.addEventListener('mouseleave', swal.resumeTimer)
        }
      })
      Toast.fire({
        icon: 'success',
        title: 'successfully'
      })
      this.getDetailOKR(this.okrid)
    })
  }
  add1(index) {
    this.Confidence.push({ Content: '', ID:0 });
  }
  add2(index) {
    this.Progress.push({ Content: '', ID:0 });
  }
  add3(index) {
    this.NextWeek.push({ Content: '', ID:0 });
  }
  remove1(ID) {
    this.http.delete(this.baseUrl + `api/okrs/DeleteOkrDetail/${ID}`).subscribe((r: any)=>{
      this.getDetailOKR(this.okrid)
    })
  }
  remove2(ID) {
    this.http.delete(this.baseUrl + `api/okrs/DeleteOkrDetail/${ID}`).subscribe((r: any)=>{
      this.getDetailOKR(this.okrid)
    })
  }
  remove3(ID) {
    this.http.delete(this.baseUrl + `api/okrs/DeleteOkrDetail/${ID}`).subscribe((r: any)=>{
      this.getDetailOKR(this.okrid)
    })
  }
}
