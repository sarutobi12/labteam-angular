import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute } from '@angular/router';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { NgbModal , NgbModalRef  } from '@ng-bootstrap/ng-bootstrap';
import  swal  from 'sweetalert2';
import { environment } from './../../../environments/environment';
@Component({
  selector: 'app-team-member',
  templateUrl: './team-member.component.html',
  styleUrls: ['./team-member.component.css']
})
export class TeamMemberComponent implements OnInit {
  modalReference: NgbModalRef
  baseUrl = environment.apiUrl
  ListProject: any [] = []
  file: File = null
  file2: File = null
  previewUrl:any = null
  fileUploadProgress: string = null
  uploadedFilePath: string = null
  previewUrl2:any = null
  fileUploadProgress2: string = null
  uploadedFilePath2: string = null
  img: any
  name:any
  position: any
  location: any
  locationEdit: any
  positionEdit: any
  nameEdit: any
  ID: any
  constructor(
    private modalService: NgbModal,
    private router: Router,
    private route: ActivatedRoute,
    private http: HttpClient,
  ) { }

  ngOnInit(): void {
    this.getListMember()
  }
  getListMember(){
    this.http.get(this.baseUrl + 'api/Teams/getall').subscribe((res: any)=>{
      this.ListProject = res
    })
  }
  save(){
    const formData = new FormData();
    formData.append('UploadedFile', this.file);
    formData.append('UploadedFileMemberName', this.name);
    formData.append('UploadedFileJobName', this.position);
    formData.append('UploadedFilePosition', this.location);
    // console.log(this.file)
    this.http.post(this.baseUrl + 'api/teams/Created',formData).subscribe((res: any)=>{
      const Toast = swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: false,
        onOpen: (toast) => {
          toast.addEventListener('mouseenter', swal.stopTimer)
          toast.addEventListener('mouseleave', swal.resumeTimer)
        }
      })
      Toast.fire({
        icon: 'success',
        title: 'New Member Add successfully'
      })
      this.getListMember();
    })
    this.modalReference.close()
  }
  EditMember(item,edit){
    this.openLgEdit(edit)
    this.ID = item.ID
    this.nameEdit = item.Name
    this.img= item.Image
    this.positionEdit = item.JobName
    this.locationEdit = item.Position
  }
  update(){
    const formData = new FormData();
    formData.append('UploadedFile', this.file2);
    formData.append('UploadedFileMemberNameEdit', this.nameEdit);
    formData.append('UploadedFileJobNameEdit', this.positionEdit);
    formData.append('UploadedFilePositionEdit', this.locationEdit);
    formData.append('UploadedFileID', this.ID);
    this.http.post(this.baseUrl + 'api/teams/Update',formData).subscribe((res: any)=>{
      const Toast = swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: false,
        onOpen: (toast) => {
          toast.addEventListener('mouseenter', swal.stopTimer)
          toast.addEventListener('mouseleave', swal.resumeTimer)
        }
      })
      Toast.fire({
        icon: 'success',
        title: 'Member Update successfully'
      })
      this.getListMember()
      this.modalReference.close()
    })
  }
  delete(ID){
    swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.http.delete(this.baseUrl + `api/teams/DeleteMember/${ID}`)
        .subscribe((res: any)=>{
          swal.fire(
            'Deleted!',
            'Member has been deleted.',
            'success'
          )
          this.getListMember()
        })
      }
    })
  }
  openLgAdd(add){
    this.modalReference = this.modalService.open(add,{ size: 'lg' })
  }
  openLgEdit(edit){
    this.modalReference = this.modalService.open(edit,{ size: 'lg' })
  }
  fileProgress(event) {
    this.file = event.target.files[0];
    this.preview();
  }
  fileProgress2(event) {
    this.file2 = event.target.files[0];
    this.preview2();
  }
  preview() {
    const mimeType = this.file.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }
    const reader = new FileReader()
    reader.readAsDataURL(this.file)
    reader.onload = (_event) => {
      this.previewUrl = reader.result
    }
  }
  preview2() {
    const mimeType = this.file2.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }
    const reader = new FileReader()
    reader.readAsDataURL(this.file2)
    reader.onload = (_event) => {
      this.previewUrl2 = reader.result
      this.img = reader.result
    }
  }
}
