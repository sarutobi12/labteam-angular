import { Component, OnInit, ViewChild, ViewEncapsulation, ElementRef } from '@angular/core';
import {Router, ActivatedRoute } from '@angular/router';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { NgbModal , NgbModalRef  } from '@ng-bootstrap/ng-bootstrap';
import  swal  from 'sweetalert2';
import { environment } from './../../../environments/environment';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { addClass, removeClass, Browser } from '@syncfusion/ej2-base';
import { ToolbarService, LinkService, ImageService, HtmlEditorService, QuickToolbarService, TableService  } from '@syncfusion/ej2-angular-richtexteditor';
import { ActionBeginEventArgs, ActionCompleteEventArgs } from '@syncfusion/ej2-angular-richtexteditor';
import { RichTextEditorComponent, RichTextEditorModel } from '@syncfusion/ej2-angular-richtexteditor';
import { ToolbarModule } from '@syncfusion/ej2-angular-navigations';
const Toast = swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000,
  timerProgressBar: false,
  onOpen: (toast) => {
    toast.addEventListener('mouseenter', swal.stopTimer)
    toast.addEventListener('mouseleave', swal.resumeTimer)
  }
})
const CLIENT_ID = '864cdf588393442'
const httpOptions = {
  headers: new HttpHeaders(
    {
      'Authorization': 'Client-ID ' + CLIENT_ID
    }
  )
}
@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css'],
  providers: [ToolbarService, LinkService, ImageService, HtmlEditorService, QuickToolbarService, TableService]
})
export class ArticleComponent implements OnInit {
  @ViewChild('valueTemplate') public rteObj: RichTextEditorComponent;
  modalReference: NgbModalRef
  baseUrl = environment.apiUrl
  img: any
  descriptionEdit: any
  nameEdit: any
  file: File = null
  file2: File = null
  previewUrl:any = null
  fileUploadProgress: string = null
  uploadedFilePath: string = null
  previewUrl2:any = null
  fileUploadProgress2: string = null
  uploadedFilePath2: string = null
  name: any
  description: any
  ListArticle: any [] = []
  contentedit: any
  ID: any
  content: any = ''
  public insertImageSettings = {
    saveUrl : 'http://10.4.0.76:94/api/Image/SaveFile',
    path: 'http://10.4.0.76:94/image/'
  }
  constructor(
    private modalService: NgbModal,
    private router: Router,
    private route: ActivatedRoute,
    private http: HttpClient
  ) { }
  ngOnInit(): void {
    this.getAllArticle()
  }
  save(){
    const formData = new FormData();
    formData.append('UploadedFile', this.file);
    formData.append('UploadedFileName', this.name);
    formData.append('UploadedFileDescription', this.description);
    formData.append('UploadedFileContent', this.content);
    this.http.post(this.baseUrl + 'api/Articles/Created2',formData).subscribe((res: any)=>{
      Toast.fire({
        icon: 'success',
        title: 'New Article Add Successfully'
      })
      this.getAllArticle();
    })
    this.modalReference.close()
  }
  editArticle(item,edit){
    this.openLgEdit(edit)
    this.ID = item.ID
    this.nameEdit = item.Name
    this.img= item.Image
    this.descriptionEdit = item.Description
    this.contentedit = item.Content
  }
  update(){
    const formData = new FormData();
    formData.append('UploadedFile', this.file2);
    formData.append('UploadedFileNameEdit', this.nameEdit);
    formData.append('UploadedFileDescriptionEdit', this.descriptionEdit);
    formData.append('UploadedFileContentEdit', this.contentedit);
    formData.append('UploadedFileID', this.ID);
    this.http.post(this.baseUrl + 'api/Articles/Updated',formData).subscribe((res: any)=>{
      Toast.fire({
        icon: 'success',
        title: 'Articles Update Successfully'
      })
        this.modalReference.close()
        this.getAllArticle();
    })
  }
  remove(ID){
    swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.http.delete(this.baseUrl + `api/Articles/DeleteArticle/${ID}`)
        .subscribe((res: any)=>{
          swal.fire(
            'Deleted!',
            'Your Article deleted successfully .',
            'success'
          )
          this.getAllArticle()
        })
      }
    })
  }
  getAllArticle(){
    this.http.get(this.baseUrl + 'api/Articles/GetAll').subscribe((res: any)=>{
        this.ListArticle = res
    })
  }
  openLgAdd(add){
    this.modalReference = this.modalService.open(add,{ size: 'xl' })
  }
  openLgEdit(edit){
    this.modalReference = this.modalService.open(edit,{ size: 'xl' })
  }
  fileProgress(event) {
    this.file = event.target.files[0];
    this.preview();
  }
  fileProgress2(event) {
    this.file2 = event.target.files[0];
    this.preview2();
  }
  preview() {
    const mimeType = this.file.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }
    const reader = new FileReader()
    reader.readAsDataURL(this.file)
    reader.onload = (_event) => {
      this.previewUrl = reader.result
    }
  }
  preview2() {
    const mimeType = this.file2.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }
    const reader = new FileReader()
    reader.readAsDataURL(this.file2)
    reader.onload = (_event) => {
      this.previewUrl2 = reader.result
      this.img = reader.result
    }
  }
}
