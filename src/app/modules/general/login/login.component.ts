import { AuthService } from './../../../_core/_services/auth.service';
import { Component, OnInit } from '@angular/core';
import { Router, RouterEvent , ActivatedRoute,NavigationEnd  } from '@angular/router';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import swal from 'sweetalert2';
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}
const Toast = swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000,
  timerProgressBar: false,
  onOpen: (toast) => {
    toast.addEventListener('mouseenter', swal.stopTimer)
    toast.addEventListener('mouseleave', swal.resumeTimer)
  }
})
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user: any = {};
  returnUrl: any;
  constructor(
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute
  ) {

  }

  ngOnInit(): void {
    // const uri = this.router.url;
    this.returnUrl = this.route.snapshot.queryParams.uri
    console.log(this.returnUrl)
    // if (this.authService.loggedIn){this.router.navigate(['/admin'])}
  }
  login(){
    this.authService.login(this.user)
      .subscribe(next =>{
        Toast.fire({
          icon: 'success',
          title: 'Login successfully'
        })
        const routerArrUser: any = [
          '/traning-resources'
        ];
        const routerArrAdmin: any = [
          '/admin-project',
          '/admin-slide',
          '/article',
          '/traning-resources',
          '/okr',
          '/admin',
          '/team-member'
        ];
        const uri = this.route.snapshot.queryParams.uri || ''
        if(this.route.snapshot.queryParams.length !== 0 )
          {
            let roleUser = routerArrUser.includes(uri);
            routerArrUser.forEach((item, index)=>{
               if(uri.includes(item)) roleUser = true;
            });
            let roleAdmin = routerArrAdmin.includes(uri);

            routerArrAdmin.forEach((item, index)=>{
               if(uri.includes(item)) roleAdmin = true;
            });
            if (Number(localStorage.getItem('role')) === 2 && roleUser ) {
              this.router.navigate([uri])
            } else if (Number(localStorage.getItem('role')) === 2 && roleAdmin) {
              this.router.navigate(['traning-resources'])
            } else if (Number(localStorage.getItem('role')) === 1 && roleAdmin  ) {
              this.router.navigate([uri])
            }
          }
          else{
            this.router.navigate(['traning-resources'])
          }
          // this.router.navigate(['/admin'])
      },error =>{
        Toast.fire({
          icon: 'error',
          title: 'Login Fail'
        })
      })
  }
}
