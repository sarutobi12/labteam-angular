import { Component, OnInit } from '@angular/core';
import { environment } from '../../../../environments/environment';
import {Router, ActivatedRoute } from '@angular/router';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { NgbModal , NgbModalRef  } from '@ng-bootstrap/ng-bootstrap';
import { TreeGridComponent } from '@syncfusion/ej2-angular-treegrid';
import { FilterService } from '@syncfusion/ej2-angular-treegrid';
import { ClipboardService } from 'ngx-clipboard'
import { DatePipe } from '@angular/common';
import  swal  from 'sweetalert2';
import { SortService,ToolbarService, PageService, EditService, ExcelExportService, PdfExportService, ContextMenuService } from '@syncfusion/ej2-angular-treegrid';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [
    SortService, DatePipe, ToolbarService, PageService, EditService, ExcelExportService, PdfExportService, ContextMenuService,FilterService
  ]
})
export class HomeComponent implements OnInit {

  name = environment.application.name;
  angular = environment.application.angular;
  bootstrap = environment.application.bootstrap;
  fontawesome = environment.application.fontawesome;
  modalReference: NgbModalRef
  public treegrid: TreeGridComponent
  baseUrl = environment.apiUrl
  ListSlide: any[] = []
  ListMember: any[] = []
  ListArticle: any[] = []
  ListProject: any[] = []
  activetab: any = 1
  imgdefault: any = 'http://10.4.0.76:94/image/image.png'
  searchSettings: any = { hierarchyMode: 'Parent' }
  contextMenuItems: any = [
    {
      text: 'Add Function Project',
      iconCss: ' e-icons e-add',
      target: '.e-content',
      id: 'Add-Sub-OC'
    },
    {
      text: 'Delete',
      iconCss: ' e-icons e-delete',
      target: '.e-content',
      id: 'DeleteOC'
    }
  ]
  toolbar: any = [
    'Search',
    'ExpandAll',
    'CollapseAll',
  ]
  editing: any = { allowDeleting: false, allowEditing: false, mode: 'Row' }
  pageSettings: any = { pageSize: 100 }
  editparams: any = { params: { format: 'n' } }
  expanded: any = {}
  data: any[] = []
  dataOkr: any[] = []
  src: any
  title: any
  Confidence: any = []
  TeamHealth: any = []
  Progress: any = []
  NextWeek: any = []
  titledetail: any
  contentdetail: any
  constructor(
    private clipboardService: ClipboardService,
    private modalService: NgbModal,
    private router: Router,
    public datePipe: DatePipe,
    private route: ActivatedRoute,
    private http: HttpClient
  ) { }

  ngOnInit() {
    this.getSlide()
    this.getListMember()
    this.getArticle()
    this.getproject()
    this.getTraining()
    this.getOKR()
  }
  showdetailnews(id,detailnew){
    this.openLgDetailNew(detailnew)
    this.http.get(this.baseUrl +`api/Articles/GetByID/${id}`).subscribe((res: any)=>{
      this.titledetail = res.Name
      this.contentdetail = res.Content
      console.log(res)
    })
  }
  watchvideo(video){
    // this.openLgVideo(video)
    this.modalReference = this.modalService.open(video,{ size: 'xl' });
  }
  OpenDetail(key,detail){
    this.openLgDetail(detail)
    this.getDetailOKR(key)
  }
  openLgDetailNew(detailnew){
    this.modalReference = this.modalService.open(detailnew,{ size: 'xl' })
  }
  openLgDetail(detail){
    this.modalReference = this.modalService.open(detail,{ size: 'xl' })
  }

  getDetailOKR(key){
    this.http.get(this.baseUrl +`api/OKRs/GetDetail/${key}`).subscribe((res: any) =>{
        this.Confidence = res.Confidence
        this.TeamHealth = res.Teamhealth
        this.Progress = res.Progess
        this.NextWeek = res.Nextweek
    })
  }

  openLgVideo(video){
    this.modalReference = this.modalService.open(video,{ size: 'xl' });
  }

  open(linkpath){
    function isValidURL(string) {
      const res = string.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
      return (res !== null)
    };
    const testCase = linkpath;
    if(isValidURL(testCase)){
      window.open(linkpath, '_blank');
    }
    else{
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Browser is not allowed to access local resources , Please copy Link Path paste to the Windows Explorer!',
      })
    }
  }
  copy(linkpath){
    this.clipboardService.copyFromContent(linkpath)
    swal.fire(
      'Copy success!',
      'copy successfully : ' + ' ' + linkpath,
      'success'
    )
  }
  actionComplete(args) {
    console.log(args)
  }
  rowSelected(args) {
    console.log(args);
    this.src = args.data.link
    console.log(this.src)
    this.title = args.data.title
  }
  loginAdmin(){
    window.open('admin', '_blank');
  }
  getTraining() {
    this.http.get(this.baseUrl + 'api/Ocs/GetListTree').subscribe((res: any) => {
      this.data = res
    })
  }
  getSlide(){
    this.http.get(this.baseUrl + 'api/Slides/GetListSlide').subscribe((res: any)=>{
      this.ListSlide = res
    })
  }
  getListMember(){
    this.http.get(this.baseUrl + 'api/Teams/getall').subscribe((res: any)=>{
      this.ListMember = res
    })
  }
  getArticle(){
    this.http.get(this.baseUrl + 'api/Articles/Getall').subscribe((res: any)=>{
      this.ListArticle = res
    })
  }
  getproject(){
    this.http.get(this.baseUrl + 'api/Projects/GetListProject').subscribe((res: any)=>{
      this.ListProject = res
    })
  }
  getOKR(){
    this.http.get(this.baseUrl + 'api/OKRs/GetListTree').subscribe((res: any) => {
      this.dataOkr = res;
    });
  }
}