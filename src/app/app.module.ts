import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { HomeComponent } from './modules/general/home/home.component';
import { NotFoundComponent } from './modules/general/not-found/not-found.component';
import { AppRoutingModule } from './app-routing.module';
import { DefaultComponent } from './layout/default/default.component';
import { SidebarComponent } from './layout/sidebar/sidebar.component';
import { SiteHeaderComponent } from './layout/site-header/site-header.component';
import { JwtModule } from '@auth0/angular-jwt';
import { LoginComponent } from './modules/general/login/login.component';
import { ProjectComponent } from './views/project/project.component';
import { ModalModule } from 'ngx-bootstrap';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { BreadcrumbComponent } from './layout/breadcrumb/breadcrumb.component';
import { CarouselComponent } from './views/carousel/carousel.component';
import { TrainingComponent } from './views/training/training.component';
import { TreeGridModule } from '@syncfusion/ej2-angular-treegrid';
import { ClipboardModule } from 'ngx-clipboard';
import { SafePipeModule } from 'safe-pipe';
import { OkrComponent } from './views/okr/okr.component';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { TeamMemberComponent } from './views/team-member/team-member.component';
import { FooterComponent } from './layout/footer/footer.component';
import { ArticleComponent } from './views/article/article.component';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { CKEditorModule } from 'ng2-ckeditor';
import { RichTextEditorAllModule } from '@syncfusion/ej2-angular-richtexteditor';
import { DialogModule } from '@syncfusion/ej2-angular-popups';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
export function tokenGetter() {
  return localStorage.getItem('token');
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NotFoundComponent,
    DefaultComponent,
    SidebarComponent,
    SiteHeaderComponent,
    LoginComponent,
    ProjectComponent,
    BreadcrumbComponent,
    CarouselComponent,
    TrainingComponent,
    OkrComponent,
    TeamMemberComponent,
    FooterComponent,
    ArticleComponent
  ],
  imports: [
    MDBBootstrapModule.forRoot(),
    DialogModule,
    RichTextEditorAllModule,
    CKEditorModule,
    AngularEditorModule ,
    TooltipModule.forRoot(),
    ClipboardModule,
    SafePipeModule,
    TreeGridModule,
    NgbModule,
    ModalModule.forRoot(),
    MatFormFieldModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    AppRoutingModule,
    // ModalModule.forRoot(),
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        whitelistedDomains: ['localhost:5000'],
        blacklistedRoutes: ['localhost:5000/api/auth']
      }
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
